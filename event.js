
var url = window.location;
var myRegexp = new RegExp(/^\/([^/]*)\//);

var Actions = {
	build: function(){
		var pathname = url.pathname.replace('/dev/build', '');
		var newURL = url.protocol + '//' + url.host + pathname + '?flush=all';
		Actions.loading('Flush');
		return newURL;
	},
	flush: function(){
		var match = myRegexp.exec(url.pathname);
		var newURL = url.protocol + '//' + url.host + '/' + match[1] + '/dev/build';
		Actions.loading('Build');
		return newURL;
	},
	loading: function(type){
		document.location.hash = type + ' in progress ';
	}

}

function KeyPress(e) {
	var evtobj = window.event? event : e
	if (evtobj.keyCode == 66 && evtobj.ctrlKey && evtobj.altKey) url.href = Actions.flush();
	if (evtobj.keyCode == 70 && evtobj.ctrlKey && evtobj.altKey) url.href = Actions.build();
}

document.onkeydown = KeyPress;